ESX = nil

local ownerVehiclePlates = {}
local ownerKeyPlates = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
	cachePlayerCars()
end)

--animation player
local dict = "anim@mp_player_intmenu@key_fob@"
Citizen.CreateThread(function()

	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(0)
	end

	while true do
		if IsControlJustPressed(1, Config.KeyboardInput) then
			lockPlayerCars()
		end
		Citizen.Wait(1)
	end
end)


RegisterCommand("krijgsleutels", function(source, args, rawCommand)
	cachePlayerCars()
end)

RegisterCommand("geeftempsleutel", function(source, args, rawCommand)
	local targetId = args[1]
	local targetPlate = ""
	for i=2, #args , 1 do
		targetPlate = targetPlate .. " " .. args[i]
	end
	if PlayerOwnsCarByPlate(targetPlate:sub(2)) then
		TriggerServerEvent("grp_solli:geeftempsleutel", targetId, targetPlate:sub(2));
		ESX.ShowNotification('Je hebt tijdelijke sleutels gegeven aan ID: '..targetId..' met kenteken: '..targetPlate:sub(2))
	else
		ESX.ShowNotification('Auto met kenteken: '..targetPlate:sub(2)..' is niet van jou.')
	end
end)


function lockPlayerCars() 
	local playerCoords = GetEntityCoords(GetPlayerPed(-1)) 
	local closeCars = ESX.Game.GetVehiclesInArea(playerCoords, Config.LockDistance)
	local playerOwnedCarsClose = {}
	-- check for owned vehicles and fill array playerOwnedCarsClose
	for i=1, #closeCars, 1 do
		local numberplate = ESX.Math.Trim(GetVehicleNumberPlateText(closeCars[i]))
		--print(numberplate)
		if PlayerOwnsCarByPlate(numberplate) or PlayerOwnsKeyByPlate(numberplate) then
			playerOwnedCarsClose[#playerOwnedCarsClose + 1] = closeCars[i]
		end
	end
	-- check lock status 
	for i=1, #playerOwnedCarsClose, 1 do
		local temp_lockStatus = GetVehicleDoorLockStatus(playerOwnedCarsClose[i])
		if temp_lockStatus == 0 then 
			-- lock vehicle
			SetVehicleDoorsLocked(playerOwnedCarsClose[i], 2)
			--close all doors
			for door=0, 4, 1 do
				SetVehicleDoorShut(playerOwnedCarsClose[i], door, false)
			end
			--show notification
			ESX.ShowNotification('Auto opslot: '..GetVehicleNumberPlateText(playerOwnedCarsClose[i])..'.')
			--sound
			PlayVehicleDoorCloseSound(playerOwnedCarsClose[i], 1)
			--key animation
			if not IsPedInAnyVehicle(PlayerPedId(), true) then
				TaskPlayAnim(PlayerPedId(), dict, "fob_click_fp", 8.0, 8.0, -1, 48, 1, false, false, false)
			end
		else 					    -- unlock vehicle
			SetVehicleDoorsLocked(playerOwnedCarsClose[i], 0)
			--show notification
			ESX.ShowNotification('Auto open: '..GetVehicleNumberPlateText(playerOwnedCarsClose[i])..'.')
			-- sound
			PlayVehicleDoorOpenSound(playerOwnedCarsClose[i], 1)
			--animation
			if not IsPedInAnyVehicle(PlayerPedId(), true) then
				TaskPlayAnim(PlayerPedId(), dict, "fob_click_fp", 8.0, 8.0, -1, 48, 1, false, false, false)
			end
		end
	end

	return 1
end

function cachePlayerCars() -- fill local array with owned plates
	ESX.TriggerServerCallback('grp_solli:cachePlayerCars', function(cb)
		temp_ownerVehiclePlates = cb
		for i = 1, #temp_ownerVehiclePlates, 1 do
			ownerVehiclePlates[i] = temp_ownerVehiclePlates[i]["plate"]
		end
		--uncommenten voor testen
		--print(dump(ownerVehiclePlates))
	end)
end

-- local trigger for getting someone elses key
RegisterNetEvent('grp_solli:addTempKey', function(targetPlate)
	table.insert(ownerKeyPlates, targetPlate)
	ESX.ShowNotification('Je hebt tijdelijke sleutels gekregen van auto: '..targetPlate)
end)


RegisterNetEvent('grp_solli:cachePlayer', function()
	cachePlayerCars();
end)
	

--check if given plate(val) is in local cached plates
function PlayerOwnsCarByPlate (val)
    for index, value in ipairs(ownerVehiclePlates) do
        if value == val then
            return true
        end
    end
    return false
end

function PlayerOwnsKeyByPlate (val)
    for index, value in ipairs(ownerKeyPlates) do
        if value == val then
            return true
        end
    end
    return false
end

--function for printing array.
--used for testing

function dump(o)
	if type(o) == 'table' then
	   local s = '{ '
	   for k,v in pairs(o) do
		  if type(k) ~= 'number' then k = '"'..k..'"' end
		  s = s .. '['..k..'] = ' .. dump(v) .. ','
	   end
	   return s .. '} '
	else
	   return tostring(o)
	end
 end
 