# GroningenRP sollicitatie

## Script voor het opslot/openen van persoonlijke voertuigen.

### Functionaliteiten
- Auto's op/van slot.
- Andere gebruikers tijdelijke sleutel geven van voertuigen.
- Configfile voor afstand autosleutel en keyboard toets op/van slot

### Commands
```
/krijgsleutels - refreshed auto sleutels.
/geeftempsleutel [ID] [Kenteken] - geeft tijdelijk reserve sleutel aan andere gebruiker tot hij/zij relogd.
```

### Dependencies

- esx_vehicleshop
- es_extended
- mysql-async