fx_version 'cerulean'
game 'gta5'

author 'Jarie Timmer'
description 'Autosloten voor GRP sollicitatie'
version '1.0'

client_scripts {  
    'client/main.lua',
    'config.lua'
}

server_scripts {
    '@mysql-async/lib/MySQL.lua', 
    'server/main.lua'
}

dependencies {
	'es_extended',
	'esx_vehicleshop',
    'mysql-async'
}