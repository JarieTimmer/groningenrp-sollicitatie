ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('grp_solli:cachePlayerCars', function(source, cb, param1)
	local xPlayer = ESX.GetPlayerFromId(source)
	MySQL.Async.fetchAll('SELECT plate FROM owned_vehicles WHERE owner = @owner', {
		['@owner'] = xPlayer.identifier
	}, function(result)
		cb(result)
	end)
end)


RegisterNetEvent('grp_solli:geeftempsleutel')

AddEventHandler("grp_solli:geeftempsleutel", function(targetId, targetPlate)
    TriggerClientEvent("grp_solli:addTempKey", targetId, targetPlate)
end)
